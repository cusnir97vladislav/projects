const tbody = document.getElementsByTagName('tbody'),
    buttonForm = document.querySelector('.bt-ane'),
    buttonClear = document.getElementById('addNewEmployee');

//GET ALL User JSON
document.addEventListener('DOMContentLoaded', getPosts);
tUsers = document.querySelector('.tabel_b');

function getPosts() {
    fetch('http://localhost:3000/user')
        .then((res) => res.json())
        .then((users) => {
            let output = `
<tr id="filter">
    <td colspan="4">
        <input type="text" class="form-control gb-search" placeholder="global search">
    </td>
</tr>
`;

            users.forEach((user) => {
                output += `
<tr class="profile">
    <td class="name-col">${user.name}</td>
    <td class="salary-col">${user.salary}</td>
    <td class="age-col">${user.age}</td>
    <td>


        <a href="#" class="btn btn-info">
            <i class="fas fa-search"></i>
        </a>

        <a href="#" class="btn btn-light edit-item" data-toggle="modal" data-target="#editModalCenter" data-id="${user.id}">
            <i class="fas fa-pencil-alt"></i>
        </a>
        <a href="#" class="btn btn-danger delete-item" data-id="${user.id}">
            <i class="fas fa-trash-alt"></i>
        </a>

    </td>
</tr>
<tr class="profile-details">
    <td colspan="4" class="details">
        <div class="details_email">${user.email}</div>
    </td>
</tr>
`;
            });

            tUsers.innerHTML = output;

            loadEventListeners();

        })

};

// Load all event listeners
function loadEventListeners() {
    // Add task event
    buttonForm.addEventListener('click', addTask);
    buttonClear.addEventListener('click', addTaskClear);
    // Global search task
    const myfilter = document.querySelector('#filter');
    myfilter.addEventListener('keyup', searchGlobal);
    //Details(email)
    tbody[0].addEventListener('click', detailsTask);
    //Edit task
    tbody[0].addEventListener('click', editTask);
    // Remove task
    tbody[0].addEventListener('click', removeTask);


};

//Add Task Clear
function addTaskClear() {
    const savName = document.getElementById('child-name');
    const savSalary = document.getElementById('child-salary');
    const savAge = document.getElementById('child-age');
    const savEmail = document.getElementById('child-email');
    savName.value = "";
    savSalary.value = "";
    savAge.value = "";
    savEmail.value = "";
}

// Search Global
function searchGlobal(e) {
    text = e.target.value.toLowerCase();
    document.querySelectorAll('.name-col').forEach(function (task) {
        const item = task.firstChild.textContent;

        if (item.toLowerCase().indexOf(text) !== -1) {
            task.parentElement.style.display = 'table-row';
        } else {
            task.parentElement.style.display = 'none';
        };
    });
};

// Details Task
function detailsTask(e) {
    if (e.target.classList.contains('btn-info')) {
        let x = e.target.parentElement.parentElement.nextElementSibling;
        if (x.className.indexOf("profile-details-show") == -1) {
            x.className += " profile-details-show";
        } else {
            x.className = x.className.replace("profile-details-show", "");
        };
    } else if (e.target.parentElement.classList.contains('btn-info')) {
        let x = e.target.parentElement.parentElement.parentElement.nextElementSibling;
        if (x.className.indexOf("profile-details-show") == -1) {
            x.className += " profile-details-show";
        } else {
            x.className = x.className.replace("profile-details-show", "");
        };
    };
};

//Edit Task
function editTask(e) {
    if (e.target.classList.contains('edit-item')) {
        let id = e.target.dataset.id;

        fetch(`http://localhost:3000/user/${id}`)
            .then((res) => res.json())
            .then((user) => {
                var userInfo = {
                    name: user.name,
                    salary: user.salary,
                    age: user.age,
                    email: user.email,
                    id: user.id
                };

                document.querySelector('#child-name-edit').value = userInfo.name;
                document.querySelector('#child-salary-edit').value = userInfo.salary;
                document.querySelector('#child-age-edit').value = userInfo.age;
                document.querySelector('#child-email-edit').value = userInfo.email;

                ///Onclick EVENT to Edit button
                let editBtn = document.querySelector('.bt-edit');
                editBtn.dataset.id = userInfo.id;

                if (editBtn.dataset.target === "true") {
                    editBtn.addEventListener('click', function () {
                        let userName = document.querySelector('#child-name-edit').value;
                        let userSalary = document.querySelector('#child-salary-edit').value;
                        let userAge = document.querySelector('#child-age-edit').value;
                        let userEmail = document.querySelector('#child-email-edit').value;
                        let userId = editBtn.dataset.id;
                        console.log(userInfo.email);
                        if (confirm('Are You Sure?')) {
                            if (userName == '' || userSalary == '' || userAge == '') {
                                alert("Completează câmpurile");
                            } else {
                                fetch(`http://localhost:3000/user/${userId}`, {
                                        headers: {
                                            'Accept': 'application/json',
                                            'Content-Type': 'application/json'
                                        },
                                        method: "PUT",
                                        body: JSON.stringify({
                                            name: userName,
                                            salary: userSalary,
                                            age: userAge,
                                            email: userInfo.email
                                        })
                                    })
                                    .then((res) => res.json())
                                    .then(() => {
                                        getPosts();
                                        console.log(userId);
                                    })
                            };
                        };
                        editBtn.dataset.target = false;
                    });
                };
            });
    };
};

// Remove Task
function removeTask(e) {
    if (e.target.parentElement.classList.contains('delete-item')) {
        let id = e.target.parentElement.dataset.id;
        if (confirm('Are You Sure?')) {
            // Make an HTTP DELETE Request
            fetch(`http://localhost:3000/user/${id}`, {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: "DELETE",

                })
                .then(() => {
                    getPosts();
                });
        };
    } else if (e.target.classList.contains('delete-item')) {
        let id = e.target.dataset.id;
        if (confirm('Are You Sure?')) {
            // Make an HTTP DELETE Request
            fetch(`http://localhost:3000/user/${id}`, {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: "DELETE",

                })
                .then(() => {
                    getPosts();
                });
        };
    };
    e.preventDefault();
};


// Add Task
function addTask(e) {

    //Save value
    const savName = document.getElementById('child-name');
    const savSalary = document.getElementById('child-salary');
    const savAge = document.getElementById('child-age');
    const savEmail = document.getElementById('child-email');
    let userName = savName.value;
    let userSalary = savSalary.value;
    let userAge = savAge.value;
    let userEmail = savEmail.value;
    if (userName == '' || userSalary == '' || userAge == '' || userEmail == '') {
        alert("Completează câmpurile");
    } else {
        fetch("http://localhost:3000/user", {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify({
                    name: userName,
                    salary: userSalary,
                    age: userAge,
                    email: userEmail
                })
            })
            .then((res) => res.json())
            .then(() => {
                addTaskClear();
                getPosts();
            })
    };
    e.preventDefault();
};
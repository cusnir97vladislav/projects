let menu = document.querySelector('.list-menu');

let list = menu.querySelectorAll('li');

list[0].querySelector('a').style.color = '#118ab2';
list[0].className += ' active';

list.forEach((element) => {
	element.querySelector('a').addEventListener('click', function() {
		let current = menu.querySelector('.active');
		current.className = '';
		current.querySelector('a').style.color = '#fff';
		this.parentElement.className += ' active';
		this.style.color = '#118ab2';
	});
});

let navbar = document.getElementById('navbar'),
	sticky = navbar.offsetTop;

window.onscroll = () =>
	window.pageYOffset >= sticky ? navbar.classList.add('sticky') : navbar.classList.remove('sticky');

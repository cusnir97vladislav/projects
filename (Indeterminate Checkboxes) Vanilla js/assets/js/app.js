document.addEventListener('DOMContentLoaded', getPosts);

const checkBoxes = document.querySelector(".section_1");

function getPosts() {
    fetch('http://localhost:3000/gradi')
        .then((res) => res.json())
        .then((users) => {
            generateTemplate(users);
            getFunctions();
        })
}


const generateTemplate = (data) => {
    let array = [],
        title = document.createElement('h1'),
        ul = document.createElement('ul'),
        li = document.createElement('li'),
        input = document.createElement('input'),
        labelEl = document.createElement('label');


    title.innerHTML = "Availabel items list";

    ul.className = 'checkboxes';
    input.setAttribute('type', 'checkbox');

    array.push(data);

    document.querySelector(".section_1").appendChild(title);
    document.querySelector(".section_1").appendChild(ul);

    array.forEach(item => {
        const container = document.querySelector(".checkboxes");
        let label = item.label;

        input.setAttribute('name', label);
        input.setAttribute('id', item.id);

        labelEl.setAttribute('for', item.id);
        labelEl.innerHTML = label;

        container.appendChild(li);
        container.querySelector('li').append(input, labelEl);

        function nivel(p, g) {
            if (p.items) {
                let newUL = document.createElement('ul');

                g.appendChild(newUL);

                p.items.forEach(l => {
                    let newLi = document.createElement('li'),
                        newInput = document.createElement('input'),
                        newLabelEl = document.createElement('label');

                    newInput.setAttribute('type', 'checkbox');
                    newInput.setAttribute('name', l.label);
                    newInput.setAttribute('id', l.id);
                    newLabelEl.setAttribute('for', l.id);
                    newLabelEl.innerHTML = l.label;
                    newUL.appendChild(newLi);
                    newLi.appendChild(newInput);
                    newLi.appendChild(newLabelEl);
                    nivel(l, newLi);
                });

            }
        }
        nivel(item, li);
    });
};

function getFunctions() {

    /*Indeterminate, check, uncheck - status */
    const checkBoxs = document.querySelectorAll('input[type="checkbox"]');
    const names = document.querySelector(".section_2");

    let namesValue = `<span style="display: flex; justify-content: space-between"><h1>Selected items list: 0</h1></span>`;

    names.innerHTML = namesValue;

    checkBoxs.forEach(element => {
        element.addEventListener("change", statusChange);
    });

    function statusChange(e) {

        let curentLevel = e.target,
            child = e.target.nextElementSibling.nextElementSibling,
            childrens = false;

        if (child != null) {
            childrens = true;
        }

        checkUncheckDuplicate(curentLevel);
        checkAll(curentLevel, childrens);
        levelElement(curentLevel);
        showSelectedNames();

    }

    /*Add arrow icon for ul*/

    const arrowElement = document.querySelectorAll("li");
    arrowElement.forEach((element) => {
        const i = document.createElement("i");
        i.classList = "fas fa-arrow-down";
        if (element.querySelector("ul") != null) {
            inputs = element.querySelector("input");
            element.insertBefore(i, inputs);
        }
    });


    let icons = document.getElementsByClassName("fas fa-arrow-down");

    // // icons.forEach(element => element.addEventListener("click", toggleList))
    for (let i = 0; i < icons.length; i++) {
        icons[i].addEventListener("click", toggleList)
    }

    /*Toggle list*/
    function toggleList(e) {
        const x = e.target.nextElementSibling.nextElementSibling.nextElementSibling;

        if (x.style.display === "none") {
            e.target.classList.remove("fa-arrow-right");
            e.target.classList.add("fa-arrow-down");
            x.style.display = "block";
        } else {
            e.target.classList.remove("fa-arrow-down");
            e.target.classList.add("fa-arrow-right");
            x.style.display = "none";
        }

    }


    const showSelectedNames = () => {

        let namesValue = `<span style="display: flex; justify-content: space-between"><h1>Selected items list: <span id="itemsNumber">0</span></h1><button id="removeAll">Remove All</button></span>`,
            checkedValue = {};
        let selectedItems = [];


        checkBoxs.forEach(element => {
            if (element.nextElementSibling.nextElementSibling === null && element.checked === true) {
                let objKey = element.nextElementSibling.textContent;
                checkedValue[`${objKey}`] = element.id;
                selectedItems.push(element.nextElementSibling.textContent);
            }
        });

        for (let key in checkedValue) {
            if (checkedValue.hasOwnProperty(key)) {

                if (checkBoxes.querySelectorAll(`input[id="${checkedValue[key]}"]`).length > 1) {
                    namesValue += `<div><span id="${checkedValue[key]}" style="color: green">${key}</span><button>REMOVE</button></div>`;

                } else {
                    namesValue += `<div><span id="${checkedValue[key]}" >${key}</span><button>REMOVE</button></div>`;
                }

            }
        }

        names.innerHTML = namesValue;
        // document.getElementById("itemsNumber").innerHTML = names.querySelectorAll("div").length;
        document.getElementById("itemsNumber").innerHTML = selectedItems.length;


        let removeButton = document.querySelector("#removeAll");

        if (selectedItems.length>1){
            removeButton.style.display = "inline-block";
        }else {
            removeButton.style.display = "none";
        }


        if (removeButton != null) {
            removeButton.style.background = "black";
            removeButton.style.color = "white";
            removeButton.style.borderRadius = "50%";
            removeButton.style.cursor = "pointer";
            removeButton.addEventListener("click", () => {
                let allInputs = document.querySelectorAll("input");
                allInputs.forEach(element => {
                    element.checked = false;
                    element.indeterminate = false;
                });
                showSelectedNames();
            });
        }

        hoverElement();
        removeCheck();
    };

    const hoverElement = () => {

        let selectedNames = names.querySelectorAll("div");

        selectedNames.forEach(element => {
            let button = element.querySelector('button');
            element.addEventListener("mouseover", () => {
                button.style.display = "inline-block";
            });
        });

        selectedNames.forEach(element => {
            let button = element.querySelector('button');
            element.addEventListener("mouseout", () => {
                button.style.display = "none";
            });
        });

    };


    const removeCheck = () => {

        let button = names.querySelectorAll("button");
        let [a, ...othersButton] = button;

        // let result = [];
        // button.forEach(element => {
        //     if(element.id!=="removeAll"){
        //         result.push(element)
        //     }
        // });


        othersButton.forEach(element => element.addEventListener("click", e =>{
            let selectedName = e.target.previousElementSibling.textContent;
            let uncheck = document.querySelectorAll(`input[name="${selectedName}"]`);
            uncheck.forEach(element => {
                element.checked = false;
                levelElement(element);
                showSelectedNames()
            });
        }));

    };

    const checkAll = (element, childrens) => {
        if (childrens === true) {
            if (element.checked) {
                // let tes = element.parentNode.querySelectorAll("ul");
                // tes.forEach(element=>element.style.display="block");
                element.nextElementSibling.nextElementSibling.querySelectorAll('input').forEach(element => {
                    element.checked = true;
                    element.indeterminate = false;

                    checkUncheckDuplicate(element);

                });
            } else {
                // let tes = element.parentNode.querySelectorAll("ul");
                // tes.forEach(element=>element.style.display="none");
                // console.log(tes);
                element.nextElementSibling.nextElementSibling.querySelectorAll('input').forEach(element => {
                    element.checked = false;
                    element.indeterminate = false;

                    checkUncheckDuplicate(element);

                });

            }
        }
        // let allInputs = checkBoxes.querySelectorAll("input"),
        //     allUl = checkBoxes.querySelectorAll("ul"),
        //     contor = 0;
        // allInputs.forEach(elemnt=>{
        //     if(elemnt.checked){
        //         contor++
        //     }
        // });
        //
        // allUl.forEach(element=>{
        //     if (element.classList.contains('checkboxes')===false && contor===0){
        //         element.style.display="none";
        //
        //     }
        // })
    };

    const levelElement = (el) => {

        let parent = el.parentNode.parentNode,
            inputParent = parent.parentNode.childNodes[1],
            childrensParent = parent.querySelectorAll('input'),
            childrensLenth = childrensParent.length,
            contor = counter(childrensLenth,childrensParent,0);

        if (contor > 0 && contor < childrensLenth) {
            inputParent.indeterminate = true;
            inputParent.checked = false;
        } else if (contor === 0) {
            inputParent.checked = false;
            inputParent.indeterminate = false;
        }

        if (contor === childrensLenth) {
            inputParent.checked = true;
            inputParent.indeterminate = false;
        }

        if (parent.classList.contains('checkboxes') !== true) {
            levelElement(parent);
        }
    };

    const checkUncheckDuplicate = (element) => {
        let inputId = element.id,
            inputState = element.checked,
            inputsById = document.querySelectorAll(`input[id="${inputId}"]`);

        if (inputsById.length > 1) {
            inputsById.forEach(element => {

                element.checked = inputState;
                levelElement(element);

            });
        }

        levelElement(element);

    };

    const counter = (a,b,c)=> {
        for (let i = 0; i < a; i++) {
            if (b[i].checked) {
                c++;
            }

        }
        return c;
    };
}


